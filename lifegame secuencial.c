
#include <pmmintrin.h>

/*C library to perform Input/Output operations*/
#include <stdio.h>
/*C  library Añade funciones para convertir texto a otro formato*/
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>

/*Libreria C para trabajar y comparar texto (de la linea de comando)*/
#include <string.h>
/* Librerias para open y write*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>


/* NOTAS
Para compilar: gcc -o lifegame.o lifegame.c -lm -fopenmp
Para ejecutar: ./lifegame.o -Tx 256 -Ty 256 -N 50 -H 1 -f salidaGrilla.raw -t 25
para ver el tiempo a ejecutar con unix: time ./lifegame.o -N 256 -T 300 -H 1 -f salidaGrilla.raw -t 300
*/

/*
 * Function principal encargada de recibir y gestionar los datos recibidos
*/
int main(int argc, char *argv[])
{
  /*Variables int guardan el archivo de salida */
  int outputF;
  /*Variables int guardan el archivo de entrada y salida respectivamente*/
  int tamanoMundoX = 0;
  int tamanoMundoY = 0;
  int num_pasos = 0;
  int iteracionSalida = 0;
  int num_hebras = 1;

  int t, j, i;

  //creo las variables para ver el tiempo transcurrido
  double start;
  double end;



  /*De tener menos de 5 elementos por parametros se cancela ya que es insuficiente para iniciar*/
  if (argc<4)
  {
    perror("se esperaban mas parametros...\n");
    return 0;
  }

  /*Se crea un loop para revisar los parametros recibidos por consola, como argc[0] es el nombre del ejecutable, se inicia en 1 para revisar del primer parametro*/
  for(int i=1; i<argc;i++)
  {
    if(strcmp(argv[i],"-Tx")==0)
    {
      /*Se verifica que el argumento posterior a -N sea un numero*/
      tamanoMundoX=atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-Ty")==0)
    {
      /*Se verifica que el argumento posterior a -N sea un numero*/
      tamanoMundoY=atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-N")==0)
    {
      /*Se verifica que el argumento posterior a -N sea un numero*/
      num_pasos=atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-H")==0)
    {
      /*Se verifica que el argumento posterior a -N sea un numero mayor a cero*/
      num_hebras=atoi(argv[i+1]);
      if(num_hebras<1)
      {
        perror("\n -H debe ser mayor a cero");
        EXIT_FAILURE;
        exit(1);
      }
    }
    if(strcmp(argv[i],"-f")==0  )
    {
      /*Se verifica que el argumento posterior abriendo o creando el archivo*/
      outputF=open(argv[i+1], O_CREAT | O_WRONLY, 0600);
      if(outputF == -1)
      {
        perror("\nFailed to create an open the file.");
        EXIT_FAILURE;
        exit(1);
      }
    }
    if(strcmp(argv[i],"-t")==0)
    {
      /*Se verifica que el argumento posterior a -N sea un numero*/
      iteracionSalida=atoi(argv[i+1]);
    }
  }/*Fin loop argc*/

  /*Se comprueba si llegaron todos los parametros obligatorios*/
  if(outputF != -1 && tamanoMundoX>0 && tamanoMundoY>0 && num_hebras>0 && iteracionSalida>0)
  {
    float mundoT1[tamanoMundoY][tamanoMundoX];//Grilla en tiempo (t-1)
    float mundo[tamanoMundoY][tamanoMundoX]; //grilla en tiempo (t) actual

    //Primero debo implantar la semilla, no es necesario usar condicionante por que el munto esta en tiempo 0
    /*
      Se deben distribuir uniformemente, por lo tanto se distribuiran X cantidad en cada cuadrante, X corresponde a la semilla y se ingresara de manera completamente aleatoria.
    */
    for( t=0;t<semilla;t++)
    {
      mundo[rand() %(tamanoMundoY-1)][rand() %(tamanoMundoX-1)] = 1;
    }
    for( t=0;t<num_pasos;t++)
    {

      //grabando cada num_Pasos
      char buf[12];
      sprintf(buf, "Salida_%d.raw", t+1);
      /*
        Creo un archivo de salida actual
      */
      int salidat=0;
      salidat=open(buf, O_CREAT | O_WRONLY, 0600);
      if(salidat == -1)
      {
        perror("\nFailed to create an open the file.");
        EXIT_FAILURE;
        exit(1);
      }


      for( i = 0; i<tamanoMundoY; i++)
      {

        for( j = 0; j<tamanoMundoX; j++)
        {
          if(t==0)//para tiempo t==0
          {

            //mundo[rand() %(tamanoMundoY-1)][rand() %(tamanoMundoX-1)] = 1;
          }
          else//else para tiempo t==0
          {
            //obtengo el numero de vecinos
            int numVecinos=0;
            if(i>0 && j>0)
            {
              numVecinos+=mundo[i-1][j-1];
            }
            if(i>0)
            {
              numVecinos+=mundo[i-1][j];
            }
            if(i>0 && j+1<tamanoMundoX)
            {
              numVecinos+=mundo[i-1][j+1];
            }
            if(j>0)
            {
              numVecinos+=mundo[i][j-1];
            }
            if(j+1<tamanoMundoX)
            {
              numVecinos+=mundo[i][j+1];
            }
            if(j>0 && i+1<tamanoMundoY)
            {
              numVecinos+=mundo[i+1][j-1];
            }
            if(i+1<tamanoMundoY)
            {
              numVecinos+=mundo[i+1][j];
            }
            if(i+1<tamanoMundoY && j+1<tamanoMundoX)
            {
              numVecinos+=mundo[i+1][j+1];
            }
            //segun el numero de vecinos actualizo el estado del personaje
            switch(numVecinos)
            {
              case 0://de 0 a 2 vecinos
              case 1:
              case 2://If a living cell has < 3 neighbors, it dies of underpopulation
                if(mundo[i][j]==1)//si esta viva muere
                {
                  mundo[i][j]=0;
                }
                break;
              case 3://If a dead cell has exactly 3 neighbors, it is "born".
                if(mundo[i][j]==0)//si esta muerta la revive
                {
                  mundo[i][j]=1;
                }
              case 4:
                //si tiene 4 la mantiene viva, no hace nada
                break;
              default:// If a living cell has > 4 neighbors, it dies of overpopulation, para el resto de valores son todos mayores a 4
                if(mundo[i][j]==1)//si esta viva muere
                {
                  mundo[i][j]=0;
                }
                break;
            }
          }//fin para tiempo t==0


          if(t==(iteracionSalida-1))//si iteracion de salida es igual al al tiempo (t) la recorro e imprimo
          {
            //  printf("\n   intentando guardar %f", grillaImpresa[i][j]);
            write(outputF, &mundo[i][j] , sizeof(float));
          }
          //grabando cada num_Pasos
          write(salidat, &mundo[i][j] , sizeof(float));


        }//fin for j
      }//fin for i

      //al final de la iteracion el mundo de tiempo (t) pasa a ser mundoT1 que corresponde a mundo en tiempo (t-1)
      //asigno num_hebras como numero de hebras para el siguiente bloque, y asigno cuales variables son compartidas y privadas.
      for( i=0;i<tamanoMundoY;i++)
      {
        for( j=0;j<tamanoMundoX;j++)
        {
          mundoT1[i][j]=mundo[i][j];
        }
      }
    }//fin for t

    close (outputF);
    //descomentar si se desea ver el tiempo empleado
    //printf("Tiempo usado con threads[N°%d] Tamano[%d] num_Pasos[%d]  Salida[%d] = %f sec.\n", num_hebras, tamanoGrilla, num_pasos, iteracionSalida, end-start);

    return 0;
  }//fin if principal
}//fin main
